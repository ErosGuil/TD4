#!/usr/bin/python3
#  -*- coding:Utf-8 -*-
import requests
from bs4 import BeautifulSoup

PROXIES = { 'https':"http://wwwcache.univ-orleans.fr:3128",'http':"http://wwwcache.univ-orleans.fr:3128"}


def get_definition(x):
    """
    Get Word definition from aonaware Site
    """
    payload = {'action':'define','dict':'wn','query':x}
    html2 = requests.get('http://services.aonaware.com/DictService/Default.aspx',params=payload).text
    soup = BeautifulSoup(html2,'lxml')
    return soup.find('pre').text

def get_definition_file(file):
    """
    Fonction permettant de récupérer les definition de mot contentue dans un fichier .txt et séparer par des sauts de ligne
    """
    lines=[]
    with open(file,'r') as f:
        for l in f.readlines():
            lines.append(l.replace('\n',''))
    with open('dictionary.txt','w') as f2:
        for word in lines :
            f2.write(get_definition(word))
            f2.write('\n')
    f2.close()

    print(lines)


get_definition_file('vocabulary.txt')