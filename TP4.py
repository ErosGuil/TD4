# encoding: utf-8

import requests
from bs4 import BeautifulSoup
#from urllib.request import urlopen
#html = urlopen("http://www.univ-orleans.fr")
# print(html.read())
#site = html.read()
PROXIES = {'https': "http://wwwcache.univ-orleans.fr:3128",
           'http': "http://wwwcache.univ-orleans.fr:3128"}
#UNI = requests.get("http://www.univ-orleans.fr",PROXIES)

#soup = BeautifulSoup(site,'lxml')
# soup = BeautifulSoup(UNI.text,'lxml')

# print(soup.h1)

# Ex 2
''' print(soup.findAll('div'))
print(soup.findAll('img'))
print(soup.findAll('link')) '''

# Ex3


def question_3():
    UNI = requests.get("https://stackoverflow.com/", PROXIES)
    soup = BeautifulSoup(UNI.text, 'lxml')
    list_question = soup.find_all('div', class_="question-summary narrow")
    liste = [getQuestionInfo(list_question[i]) for i in range(0, 10)]
    export_csv(liste)
    pass


def getQuestionInfo(question):
    question.findAll('div', class_="votes")
    votes = int(question.findAll('div', class_="votes")[0].span.text)
    answers = int(question.findAll(
        'div', class_="status unanswered")[0].span.text)
    views = int(question.findAll('div', class_="views")[0].span.text)
    text = str(question.h3.a.text)
    tags = [str(tag.text)
            for tag in question.find('div', class_='tags').findAll('a')]
    return {"votes": votes, "answers": answers, "views": views, "text": text, "tags": tags}


def export_csv(liste):
    from csv import DictWriter
    with open('stack_Q.csv', 'w') as outfile:
        writer = DictWriter(
            outfile, ('text', 'votes', 'answers', 'views', 'tags'))
        writer.writeheader()
        writer.writerows(liste)
# question_3()
